const mysql = require('mysql');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'developpeur1',
    password: 'DeveloppeurIonic',
    database: 'bdd_hero'
});

db.connect((err) => {
    if (err) {
        console.error('Erreur de connexion à la base de données :', err);
        return;
    }
    console.log('Connecté à la base de données MySQL');
});

module.exports = db;
