const express = require('express');
const router = express.Router();
const db = require('../db');

router.get('/collectionneur', (req, res) => {
    db.query('SELECT * FROM succes WHERE nom LIKE "collection%"', (error, results) => {
        if (error) {
            console.error('Erreur lors de la récupération des informations :', error);
            res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
        } else {
            res.json(results);
        }
    });
});

router.get('/chasseur', (req, res) => {
    db.query('SELECT * FROM succes WHERE nom LIKE "chasseur%"', (error, results) => {
        if (error) {
            console.error('Erreur lors de la récupération des informations :', error);
            res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
        } else {
            res.json(results);
        }
    });
});

router.get('/explorateur', (req, res) => {
    db.query('SELECT * FROM succes WHERE nom LIKE "explorateur%"', (error, results) => {
        if (error) {
            console.error('Erreur lors de la récupération des informations :', error);
            res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
        } else {
            res.json(results);
        }
    });
});

router.get('/fins', (req, res) => {
    db.query('SELECT * FROM succes WHERE nom LIKE "fin%"', (error, results) => {
        if (error) {
            console.error('Erreur lors de la récupération des informations :', error);
            res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
        } else {
            res.json(results);
        }
    });
});

// Export du routeur
module.exports = router;