const express = require('express');
const router = express.Router();
const db = require('../db');

// Récupère tous les équipements équipés par le personnage.
router.get('/equipement/equipe/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT * FROM Equipement RIGHT JOIN EquipementPersonnage ON Equipement.equipementID = EquipementPersonnage.EquipementID WHERE EquipementPersonnage.personnageID ='+id+' AND Equipement.isEquipe = 1', (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      res.json(results);
    }
  });
});

// Récupère les casques possédés par le personnage.
router.get('/equipement/casque/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT * FROM Equipement RIGHT JOIN EquipementPersonnage ON Equipement.equipementID = EquipementPersonnage.EquipementID WHERE EquipementPersonnage.personnageID ='+id+' AND Equipement.typeEquipement = "Casque"', (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      res.json(results);
    }
  });
});

// Récupère les armures possédés par le personnage.
router.get('/equipement/armure/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT * FROM Equipement RIGHT JOIN EquipementPersonnage ON Equipement.equipementID = EquipementPersonnage.EquipementID WHERE EquipementPersonnage.personnageID ='+id+' AND Equipement.typeEquipement = "Armure"', (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      res.json(results);
    }
  });
});

// Récupère les armes possédés par le personnage.
router.get('/equipement/arme/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT * FROM Equipement RIGHT JOIN EquipementPersonnage ON Equipement.equipementID = EquipementPersonnage.EquipementID WHERE EquipementPersonnage.personnageID ='+id+' AND Equipement.typeEquipement = "Arme"', (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      res.json(results);
    }
  });
});

// Récupère les boucliers possédés par le personnage.
router.get('/equipement/bouclier/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT * FROM Equipement RIGHT JOIN EquipementPersonnage ON Equipement.equipementID = EquipementPersonnage.EquipementID WHERE EquipementPersonnage.personnageID ='+id+' AND Equipement.typeEquipement = "Bouclier"', (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      res.json(results);
    }
  });
});

// Récupère les bottes possédés par le personnage.
router.get('/equipement/bottes/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT * FROM Equipement RIGHT JOIN EquipementPersonnage ON Equipement.equipementID = EquipementPersonnage.EquipementID WHERE EquipementPersonnage.personnageID ='+id+' AND Equipement.typeEquipement = "Bottes"', (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      res.json(results);
    }
  });
});

// Récupère les statistiques du personnage (pdv, atk, def).
router.get('/stats/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT pdv, atk, def FROM Info LEFT JOIN Personnage ON Info.infoID = Personnage.infoID WHERE Personnage.personnageID ='+id, (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      res.json(results);
    }
  });
});

// Crée les info d'un personnage (pdv, atk, def) et récupère l'id de l'élément créé.
router.post('/creation/info', (req, res) => {
  const { pdv, atk, def } = req.body;
  const query = 'INSERT INTO Info (infoID, pdv, atk, def) VALUES (DEFAULT, ?, ?, ?)';
  db.query(query, [pdv, atk, def], (error, results) => {
    if (error) {
      console.error('Erreur lors de l\'insertion des informations :', error);
      res.status(500).json({ message: 'Erreur lors de l\'insertion des informations' });
    } else {
      const insertedId = results.insertId;
      res.json(insertedId);
    }
  });
});

// Crée un personnage (sexe, argent, infoID, lieuID, cheminImg, nom, equipementPersonnageID).
router.post('/creation', (req, res) => {
  const { sexe, argent, infoID, lieuID, cheminImg, nom } = req.body;
  const query = 'INSERT INTO Personnage (personnageID, sexe, argent, infoID, lieuID, cheminImg, nom) VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)';
  db.query(query, [sexe, argent, infoID, lieuID, cheminImg, nom], (error, results) => {
    if (error) {
      console.error('Erreur lors de l\'insertion des informations :', error);
      res.status(500).json({ message: 'Erreur lors de l\'insertion des informations' });
    } else {
      const personnageID = results.insertId;

      // Récupère toutes les informations du personnage.
      const selectQuery = 'SELECT * FROM Personnage WHERE personnageID = ?';
      db.query(selectQuery, [personnageID], (selectError, selectResults) => {
        if (selectError) {
          console.error('Erreur lors de la récupération des informations du personnage :', selectError);
          res.status(500).json({ message: 'Erreur lors de la récupération des informations du personnage' });
        } else {
          const personnage = selectResults[0];
          res.json(personnage);
        }
      });
    }
  });
});

// Update le boolean isEquipe de true à false
router.post('/equipement/equipeToFalse/:id', (req, res) => {
  const id = req.params.id;
  const query = 'UPDATE Equipement SET isEquipe = false WHERE equipementID ='+id;
  db.query(query, (error, results) => {
    if (error) {
      console.error('Erreur lors de la mise à jour des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la mise à jour des informations' });
    } else {
      res.json(results);
    }
  });
});


// Update le boolean isEquipe de false à true
router.post('/equipement/equipeToTrue/:id', (req, res) => {
  const id = req.params.id;
  const query = 'UPDATE Equipement SET isEquipe = true WHERE equipementID ='+id;
  db.query(query, (error, results) => {
    if (error) {
      console.error('Erreur lors de l\'insertion des informations :', error);
      res.status(500).json({ message: 'Erreur lors de l\'insertion des informations' });
    } else {
      res.json(results);
    }
  });
});

// Ajoute l'id du joueur en base dans la table personnageJoueur et l'id du joueur.
router.post('/creation/liaisonToJoueur', (req, res) => {
  const { idPersonnage, idJoueur } = req.body;
  const query = 'INSERT INTO PersonnageJoueur (personnageJoueurID, personnageID, joueurID) VALUES (DEFAULT, ?, ?)';
  db.query(query, [idPersonnage, idJoueur], (error, results) => {
    if (error) {
      console.error('Erreur lors de l\'insertion des informations :', error);
      res.status(500).json({ message: 'Erreur lors de l\'insertion des informations' });
    } else {
      const insertedId = results.insertId;
      res.json(insertedId);
    }
  });
});

// Récupère le personnage grâce à son id.
router.get('/mesPersonnages/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT * FROM Personnage WHERE personnageID ='+id, (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      res.json(results);
    }
  });
});

// Export du routeur
module.exports = router;