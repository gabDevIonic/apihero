const express = require('express');
const router = express.Router();
const db = require('../db');

// Crée un joueur (joueurID, mail).
router.post('/creation', (req, res) => {
  const mail = req.body.email; // Utiliser la clé "email" au lieu de "mail"
  if (!mail) {
    res.status(400).json({ message: 'Le champ "mail" ne peut pas être vide.' });
    return;
  }
  const query = 'INSERT INTO Joueur (joueurID, mail) VALUES (DEFAULT, ?)';
  db.query(query, [mail], (error, results) => {
    if (error) {
      console.error('Erreur lors de l\'insertion des informations :', error);
      res.status(500).json({ message: 'Erreur lors de l\'insertion des informations' });
    } else {
      const joueurID = results.insertId;

      // Récupère toutes les informations du personnage.
      const selectQuery = 'SELECT * FROM Joueur WHERE joueurID = ?';
      db.query(selectQuery, [joueurID], (selectError, selectResults) => {
        if (selectError) {
          console.error('Erreur lors de la récupération des informations du personnage :', selectError);
          res.status(500).json({ message: 'Erreur lors de la récupération des informations du personnage' });
        } else {
          const joueur = selectResults[0];
          res.json(joueur);
        }
      });
    }
  });
});

// Récupère la liste des personnages du joueur.
router.get('/personnage/:id', (req, res) => {
  const id = req.params.id;
  db.query('SELECT Personnage.nom FROM Personnage RIGHT JOIN PersonnageJoueur ON Personnage.personnageID = PersonnageJoueur.personnageID WHERE PersonnageJoueur.joueurID = '+id, (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      const personnages = results.map((result) => result.personnageID);
      console.log(personnages);
      res.json(personnages);
    }
  });
});

// Récupère l'id du joueur.
router.get('/user/:mail', (req, res) => {
  const mail = req.params.mail;
  db.query(`SELECT * FROM Joueur WHERE mail = '${mail}'`, (error, results) => {
    if (error) {
      console.error('Erreur lors de la récupération des informations :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
    } else {
      const personnage = results;
      res.json(personnage);
    }
  });
});

// Export du routeur
module.exports = router;