const express = require('express');
const router = express.Router();
const db = require('../db');

router.get('/', (req, res) => {
    db.query('SELECT * FROM equipement', (error, results) => {
        if (error) {
            console.error('Erreur lors de la récupération des informations :', error);
            res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
        } else {
            res.json(results);
        }
    });
});

// Export du routeur
module.exports = router;