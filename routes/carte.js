const express = require('express');
const router = express.Router();
const db = require('../db');

router.get('/boss/:texte', (req, res) => {
    const texte = req.params.texte;
    db.query('SELECT histoireID FROM Histoire WHERE texte LIKE ?', [`%${texte}%`], (error, results) => {
        if (error) {
            console.error('Erreur lors de la récupération des informations :', error);
            res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
        } else {
            res.json(results);
        }
    });
});

router.get('/ville/:texte', (req, res) => {
    const texte = req.params.texte;
    db.query('SELECT histoireID FROM Histoire WHERE texte LIKE ?', [`%${texte}%`], (error, results) => {
        if (error) {
            console.error('Erreur lors de la récupération des informations :', error);
            res.status(500).json({ message: 'Erreur lors de la récupération des informations' });
        } else {
            res.json(results);
        }
    });
});


// Export du routeur
module.exports = router;