const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const app = express();
const port = 3000;

app.use(express.json())

const db = mysql.createConnection({
    host: 'localhost',
    user: 'developpeur1',
    password: 'DeveloppeurIonic',
    database: 'bdd_hero'
});

// Connexion à la base de données MySQL
db.connect((err) => {
    if (err) {
        console.error('Erreur de connexion à la base de données :', err);
        return;
    }
    console.log('Connecté à la base de données MySQL');
});

app.use(cors());

// Import des fichiers de routes
const equipementRoutes = require('./routes/equipement');
const monstreRoutes = require('./routes/monstre');
const lieuRoutes = require('./routes/lieu');
const infoRoutes = require('./routes/info');
const succesRoutes = require('./routes/succes');
const personnageRoutes = require('./routes/personnage');
const carteRoutes = require('./routes/carte');
const histoireRoutes = require('./routes/histoire');
const joueurRoutes = require('./routes/joueur');

// Utilisation des routeurs
app.use('/equipement', equipementRoutes);
app.use('/monstre', monstreRoutes);
app.use('/lieu', lieuRoutes);
app.use('/info', infoRoutes);
app.use('/succes', succesRoutes);
app.use('/personnage', personnageRoutes);
app.use('/carte', carteRoutes);
app.use('/histoire', histoireRoutes);
app.use('/joueur', joueurRoutes);

app.listen(port, () => {
    console.log(`Serveur démarré sur le port ${port}`);
  });
  